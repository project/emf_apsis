<?php

/**
 * @file
 * Apsis API custom soap client
 *
 * @author Hasse Ramlev Hansen <hasse@reload.dk>
 */

/**
 * Include NUSOAP classes
 */
if (file_exists(libraries_get_path('nusoap') .'/nusoap.php')) {
  require_once(libraries_get_path('nusoap') .'/nusoap.php');
}

class AnpSoapClientException extends Exception {
  public function __construct($message = NULL, $code = 0) {
    parent::__construct($message, $code);
  }
}

/**
 * NUSOAP wrapper for ANP API
 */
class AnpSoapclient {
  protected $service_username = NULL, $service_password = NULL;
  protected $soap_client = NULL;

  /**
   * Class constructor
   */
  public function __construct($username, $password, $wsdl = 'http://api.anpdm.com/ExternalAPIService.asmx?wsdl') {
    // Init soapclient
    $this->soap_client = $soap_client = new nusoap_client($wsdl, 'wsdl');
    if ($soap_error = $soap_client->getError()) {
      $this->ThrowError($soap_error, -1, TRUE);
    }

    // Set authentication
    $this->SetAuthenticationDetails($username, $password);
  }

  /**
   * Call soap client
   *
   * @param $method
   * @param array $arguments
   * @return object resultset
   */
  public function Call($method, $arguments = array()) {
    // Init
    $soap_client = $this->soap_client;

    // Append anp username and password to parameters vector
    $arguments = array_merge(array('strUsername' => $this->service_username, 'strPassword' => $this->service_password), (array) $arguments);
    $soap_result = @$soap_client->Call($method, array('parameters' => $arguments), '', '', FALSE, TRUE);

    // Print error/return result
    return ($soap_client->fault ? $this->ThrowError($soap_result, -2, TRUE) :
      (($soap_error = $soap_client->getError()) ? $this->ThrowError($soap_error, -3, TRUE) : $soap_result));
  }

  /**
   * Throw error
   *
   * @param string $description
   * @param integer $error_code
   * @param string $dump_description
   * @return void
   */
  protected function ThrowError($description, $error_code = -1, $dump_description = FALSE) {
    if ($dump_description) {
      @ob_start(); print_r($description);
      $description = @str_replace(chr(32) . chr(32), "&nbsp;", @nl2br(@ob_get_clean()));
    }
    throw new AnpSoapClientException($description, $error_code);
  }

  /**
   * Check and set authentication details
   *
   * @param string username
   * @param string password
   * @return void
   */
  protected function SetAuthenticationDetails($username, $password) {
    if ((drupal_strlen($username) == 0) || (drupal_strlen($password) == 0) || $username == NULL || $password == NULL) {
      $this->ThrowError("Username or Password empty!", -5);
    }

    $this->service_username = $username;
    $this->service_password = $password;
  }
}

/**
 * Demographics class
 */
class AnpDemographicsData extends BaseDictionary {

  /**
   * Call constructor
   */
  public function __construct($demographics_data = NULL) {

     // Call base constructor
    parent::__construct();

    // Populate demographics data
    if ($demographics_data != NULL && count($demographics_data) > 0)
      foreach ($demographics_data as $key => $val) {
        $this->Add($key, $val);
      }
  }

  /**
   * Get keys
   *
   * @param string $delim
   * @return string
   */
  public function GetKeys($delim = ';') {
    return implode($delim, array_keys($this->dictionary));
  }

  /**
   * Get values
   *
   * @param string $delim
   * @return string
   */
  public function GetValues($delim = ';') {
    return implode($delim, array_values($this->dictionary));
  }

  /**
   * Wrapper for base Remove function
   * @param string $key
   * @return void
   */
  public function Remove($key) {
    parent::Remove("DD" . $key);
  }

  /**
   * Wrapper for base Add function
   * @param string $key
   * @param string $value
   * @return void
   */
  public function Add($key, $value) {
    parent::Add("DD" . $key, $value);
  }
}

/**
 * Dictionary class
 */
class BaseDictionary {
  protected $dictionary = NULL;

  /**
   * Call constructor
   */
  public function __construct() {
    $this->dictionary = array();
  }

  /**
   * add key to dictionary
   *
   * @param string $key
   * @param string $value
   * @return void
   */
  public function Add($key, $value) {
    if ($this->ContainsKey($key)) throw new Exception("Dictionary already contains key {$key}");
    $this->dictionary[$key] = $value;
  }

  /**
   * Remove key from dictionary
   *
   * @param string $key
   */
  public function Remove($key) {
    unset($this->dictionary[$key]);
  }

  /**
   * Get value from distionary
   *
   * @param string $key
   * @return string
   */
  public function Get($key) {
    return $this->dictionary[$key];
  }

  /**
   * Check for key in dictionary
   *
   * @param $key
   * @return boolean
   */
  public function ContainsKey($key) {
    return array_key_exists($key, $this->dictionary);
  }
}

